@extends('layouts.master')

@section('top')
@endsection

@section('content')
    <!-- general form elements -->
    <div class="box-header">
        <a href="{{ route('users.index') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> BACK</a>
    </div>
    @if ($user->role !== 'superadmin')
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Edit User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('users.update', ['id' => $user->id])  }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <div class="box-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $user->name }}" id="name" class="form-control" required>
                        @if ($errors->has('name'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" name="email"  value="{{ $user->email  }}" id="email" class="form-control"  required>
                        @if ($errors->has('email'))
                            <br>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="alert alert-info" role="alert">
                            Leave the password field empty if you don't want to change.
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                        @if ($errors->has('password'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password">
                        @if ($errors->has('confirm_password'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('confirm_password') }}
                            </div>
                        @endif
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> UPDATE</button>
                </div>
            </form>
        </div>
        <!-- /.box -->
    @endif
@endsection

@section('bot')
@endsection
